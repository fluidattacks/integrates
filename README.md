# Fluid Attacks / Product repository

## Integrates

[![Language grade: Python](https://img.shields.io/lgtm/grade/python/g/fluidattacks/integrates.svg?logo=lgtm&logoWidth=18)](https://lgtm.com/projects/g/fluidattacks/integrates/context:python)
[![Language grade: TypeScript](https://img.shields.io/lgtm/grade/javascript/g/fluidattacks/integrates.svg?logo=lgtm&logoWidth=18)](https://lgtm.com/projects/g/fluidattacks/integrates/context:python)
[![Quality Gate](https://sonarcloud.io/api/project_badges/measure?project=fluidattacks_integrates&metric=alert_status)](https://sonarcloud.io/dashboard?id=fluidattacks_integrates)
[![Coverage](https://codecov.io/gl/fluidattacks/integrates/branch/master/graph/badge.svg)](https://codecov.io/gl/fluidattacks/integrates)
[![Build](https://gitlab.com/fluidattacks/integrates/badges/master/pipeline.svg)](https://gitlab.com/fluidattacks/integrates/-/commits/master)

## Reviews

[![License](https://img.shields.io/pypi/l/skims)](../LICENSE)
[![Docs](https://img.shields.io/badge/Docs-grey)](./reviews/README.md)

## Skims

[![PyPI](https://img.shields.io/pypi/v/skims)](https://pypi.org/project/skims)
[![Status](https://img.shields.io/pypi/status/skims)](https://pypi.org/project/skims)
[![Downloads](https://img.shields.io/pypi/dm/skims)](https://pypi.org/project/skims)
[![License](https://img.shields.io/pypi/l/skims)](../LICENSE)
[![Docs](https://img.shields.io/badge/Docs-grey)](./skims/README.md)
