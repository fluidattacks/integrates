import ./fetch-src.nix {
  repo = "https://github.com/NixOS/nixpkgs";
  commit = "2cd2e7267e5b9a960c2997756cb30e86f0958a6b";
  digest = "0ir3rk776wldyjz6l6y5c5fs8lqk95gsik6w45wxgk6zdpsvhrn5";
}
