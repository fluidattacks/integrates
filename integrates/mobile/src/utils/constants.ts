/**
 * Note about security concerns
 *
 * All the following constants are just client ids.
 * They are not meant to be secret.
 * Their security relies on a whitelist of
 * origins and redirect urls
 */
export const BUGSNAG_KEY: string = "c7b947a293ced0235cdd8edc8c09dad4";
export const GOOGLE_LOGIN_KEY_ANDROID_DEV: string =
  "335718398321-vv3cfdee0ng40tuhgm5g2mp42c2d2o9j.apps.googleusercontent.com";
export const GOOGLE_LOGIN_KEY_ANDROID_PROD: string =
  "335718398321-lkf186ev7fujqmfe59bb05oehn4l2g5c.apps.googleusercontent.com";
export const GOOGLE_LOGIN_KEY_IOS_DEV: string =
  "335718398321-5b0vjrl7gmj1dk3c0ma02q6lpqg5r57u.apps.googleusercontent.com";
export const GOOGLE_LOGIN_KEY_IOS_PROD: string =
  "335718398321-s67kko7rj2cdf1a3jaj1vgdohjme2sna.apps.googleusercontent.com";
export const MICROSOFT_LOGIN_KEY: string =
  "4a9923e0-17f1-43b7-9b16-7892eed25f18";
export const BITBUCKET_LOGIN_KEY_DEV: string = "ht82hTFNUYABLSvd8M";
export const BITBUCKET_LOGIN_SECRET_DEV: string =
  "2MLSTvREBFXb9sQ5TbLnbAVffPxVb8Aw";
export const BITBUCKET_LOGIN_KEY_PROD: string = "2LCcqgAW3zyGMpHkx7";
export const BITBUCKET_LOGIN_SECRET_PROD: string =
  "pfpHhnWUaVbQRp2szgbTTYKNz7C9zRg9";
