import { StyleSheet } from "react-native";

export const styles: Dictionary = StyleSheet.create({
  container: {
    alignItems: "center",
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
  },
  icon: {
    height: 50,
    marginLeft: 10,
    tintColor: "#FE3435",
    width: 50,
  },
});
