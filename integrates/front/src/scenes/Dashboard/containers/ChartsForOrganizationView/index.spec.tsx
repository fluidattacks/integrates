import { ChartsForOrganizationView } from "./index";

describe("ChartsForOrganizationView", () => {

  it("should return an function", () => {
    expect(typeof (ChartsForOrganizationView))
      .toEqual("function");
  });

});
