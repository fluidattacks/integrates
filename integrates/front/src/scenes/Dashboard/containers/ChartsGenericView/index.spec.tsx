import { ChartsGenericView } from "./index";

describe("ChartsGenericView", () => {

  it("should return an function", () => {
    expect(typeof (ChartsGenericView))
      .toEqual("function");
  });

});
