# Tap-git

This is the Extract and Transform component of a git repo ET&L.

## Getting started

First create a JSON configuration file acording to the example.

Then run a tap and a target:

```
tap-git --conf config.json | target-anysingertarget
```
