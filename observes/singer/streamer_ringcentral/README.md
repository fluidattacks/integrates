# Streamer RingCentral

Reads the [Ring Central API](https://developer.ringcentral.com/),
and persist it to stdout as a JSON formatted stream.

# How to install
Download a fresh copy of the source code and install it with pip:

```bash
$ cd tap_ringcentral
$ python3 -m pip install .
```

Installation requires python 3.6 or later.

# Sponsor

[![Fluid attacks logo][logo]](https://fluidattacks.com/)

[logo]: https://fluidattacks.com/web/theme/images/logo.png
