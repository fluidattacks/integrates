# Tap Formstack

Reads the [Formstack's API](https://www.formstack.com/),
and persists it to a [Singer](https://www.singer.io/) formatted stream.

# How to install
Download a fresh copy of the source code and install it from source:

```bash
$ cd tap-formstack
$ python3 -m pip install .
```

Installation requires python 3.6 or later.

# Sponsor

[![Fluid attacks logo][logo]](https://fluidattacks.com/)

[logo]: https://fluidattacks.com/web/theme/images/logo.png
