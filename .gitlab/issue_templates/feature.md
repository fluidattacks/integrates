<!--
Issues are public, and therefore should not contain confidential information.
-->

<!--
The first four sections are strongly recommended:
- Problem to solve
- Intended users
- User experience goal
- Proposal

The rest of the sections can be filled out during the problem validation or
breakdown phase.

However, keep in mind that providing complete and relevant information early
helps our product team validate the problem and start working on a solution.
-->

### Problem to solve

<!--
What problem do we solve?
Try to define the who/what/why of the opportunity as a user story.
For example, "As a (who), I want (what), so I can (why/value)."
-->

### Intended users

<!--
Who will use this feature? If known, include any of the following:
- types of users/roles (e.g. Developer)
- specific company roles (e.g. Release Manager).
It's okay to write "Unknown" and fill this field in later.
-->

### User experience goal

<!--
What is the single user experience workflow this problem addresses?
For example: "The user should be able to use the UI/API to <perform a specific task>"
-->


### Proposal

<!--
How are we going to solve the problem?
-->

### Permissions and Security

<!--
What permissions are required to perform the described actions?
Are they consistent with the existing permissions as documented for users,
groups, organizations as appropriate?

Is the proposed behavior consistent between the UI, API,
and other access methods?
-->

### What does success look like, and how can we measure that?

<!--
Define both the success metrics and acceptance criteria.

Note that success metrics indicate the desired business outcomes,
while acceptance criteria indicate when the solution is working correctly.
-->

### Links / references

/label ~feature
